const User = require('../models/User')
const Course = require('../models/Course')
const bcrypt = require('bcrypt')
const auth = require('../auth')

//Check if the email already exists
module.exports.checkEmailExists = (reqBody)=>{
	return User.find({email:reqBody.email}).then(result=>{
		if(result.length > 0){
// 			return `
// ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣤⣤⣤⣤⣤⣶⣦⣤⣄⡀⠀⠀⠀⠀⠀⠀⠀⠀  ____________________________
// ⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⡿⠛⠉⠙⠛⠛⠛⠛⠻⢿⣿⣷⣤⡀⠀⠀⠀⠀⠀/${reqBody.email}
// ⠀⠀⠀⠀⠀⠀⠀⠀⣼⣿⠋⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⠈⢻⣿⣿⡄⠀⠀⠀⠀ |
// ⠀⠀⠀⠀⠀⠀⠀⣸⣿⡏⠀⠀⠀⣠⣶⣾⣿⣿⣿⠿⠿⠿⢿⣿⣿⣿⣄⠀⠀⠀| 𝐈𝐒 𝐒𝐔𝐒
// ⠀⠀⠀⠀⠀⠀⠀⣿⣿⠁⠀⠀⢰⣿⣿⣯⠁⠀⠀⠀⠀⠀⠀⠀⠈⠙⢿⣷⡄⠀/へ.＿＿＿＿＿＿＿______________
// ⠀⠀⣀⣤⣴⣶⣶⣿⡟⠀⠀⠀⢸⣿⣿⣿⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣷⠀
// ⠀⢰⣿⡟⠋⠉⣹⣿⡇⠀⠀⠀⠘⣿⣿⣿⣿⣷⣦⣤⣤⣤⣶⣶⣶⣶⣿⣿⣿⠀
// ⠀⢸⣿⡇⠀⠀⣿⣿⡇⠀⠀⠀⠀⠹⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠃⠀
// ⠀⣸⣿⡇⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠉⠻⠿⣿⣿⣿⣿⡿⠿⠿⠛⢻⣿⡇⠀⠀
// ⠀⣿⣿⠁⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣧⠀⠀
// ⠀⣿⣿⠀⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⠀⠀
// ⠀⣿⣿⠀⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⠀⠀
// ⠀⢿⣿⡆⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⡇⠀⠀
// ⠀⠸⣿⣧⡀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣿⠃⠀⠀
// ⠀⠀⠛⢿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⣰⣿⣿⣷⣶⣶⣶⣶⠶⢠⣿⣿⠀⠀⠀
// ⠀⠀⠀⠀⠀⠀⠀⣿⣿⠀⠀⠀⠀⠀⣿⣿⡇⠀⣽⣿⡏⠁⠀⠀⢸⣿⡇⠀⠀⠀
// ⠀⠀⠀⠀⠀⠀⠀⣿⣿⠀⠀⠀⠀⠀⣿⣿⡇⠀⢹⣿⡆⠀⠀⠀⣸⣿⠇⠀⠀⠀
// ⠀⠀⠀⠀⠀⠀⠀⢿⣿⣦⣄⣀⣠⣴⣿⣿⠁⠀⠈⠻⣿⣿⣿⣿⡿⠏⠀⠀⠀⠀
// ⠀⠀⠀⠀⠀⠀⠀⠈⠛⠻⠿⠿⠿⠿⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
// 			`
			return true
		}
		else{
			return false
			
		}
	})
}

//User Registration
module.exports.registerUser = (reqBody)=>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10) //bcrypt hashing
	})
	//Saves the created object to our database
	return newUser.save().then((user,error)=>{
		//User registration failed
		if(error){
			return false
		}
		//User registration successful
		else{
			return true
		}
	})
}

//User authentication
/*
Steps:
1. Check the database if the user the user email exists
2. Compare the password provided the login form with the password stored in the database
3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result=>{
		//If user does not exist
		if(result == null){
			return false
		}
		else{
			//user exists
			//Create a variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			//"compareSync" method is used to compare a non-encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			//A good practice for boolean variable is to use the word "is" or "are"
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			//If the passwords match
			if(isPasswordCorrect){
				//Generate an access token
				//Use the "createAccessToken" method defined in the "auth.js" file
				//returning an object back to the frontend
				//We will use the mongoose method "toObject" to convert the mongoose object into plain javascript object
				return {accessToken:auth.createAccessToken(result.toObject())}
			}
			else{
				//Passwords do not match
				return false
			} 
		}
	})
}

//s33
// module.exports.getProfile = (reqBody)=>{
// 	return User.findById({_id:reqBody.id}).then((result,err)=>{
// 		//NOTE: in Postman, use "id" NOT "_id"
// 		if(err){
// 			console.log(err)
// 			return false
// 		}
// 		else{
// 			result.password = ""
// 			return result
// 		}
// 	})
// }


//s33 can be shortened to
// module.exports.getProfile = (reqBody)=>{
// 	return User.findById({_id:reqBody.id}).then((result,err)=>{
// 		result.password = ""
// 		return result
// 	})
// }

//s33 maam judy version
module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result => {
		result.password = "";
		return result;
	})
}

//Enroll a user to a class/course
/*
Steps:
1. Find the document in the database using the user's ID
2. Add the course ID to the user's enrollment array
3. Save the data in the Database
4. Find the document in the database using the course's ID
5. Add the User ID to the course's enrollees array
6. Save the data in the database
7, Error handling: if successful, return true, else return false
*/

//Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user

module.exports.enroll= async (data)=>{
	//Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	//Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the client
	let isUserUpdated = await User.findById(data.userId).then(user =>{
		//Adds the courseId in the user's enrollements array
		user.enrollments.push({courseId:data.courseId})
	
		//Save the updated user information in the database
		return user.save().then((user,error)=>{
			if(error){
				return false
			}
			else{
				return true
			}
		})//isUserUpdated tells us if the course was successfully added to the user
	})
	
	let isCourseUpdated = await Course.findById(data.courseId).then(course=>{
		//Add the userId in the course's enrollees array
		course.enrollees.push({userId:data.userId})

		return course.save().then((course,error)=>{
			if(error){
				return false
			}
			else{
				return true
			}			
		})//the isCourseUpdated tells us if the user was successfully saved to the course
	})

	//Condition that will check if the user and course documents have been successfully updated
	if(isUserUpdated && isCourseUpdated){
		return true
	}
	else{//if user enrollment failed
		return false
	}
}