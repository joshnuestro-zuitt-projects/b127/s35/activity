const Course = require('../models/Course')

//Creation of Course
/*
Steps
1. Create a conditional statement that will check if the user is an admin
2. Create a new Course object using the mongoose model and the information from the request body
3. Save the new Course to the database
*/

module.exports.addCourse = (data)=>{
	//If user is an admin
	if(data.isAdmin){
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})
		return newCourse.save().then((course,error)=>{
			//Course creation failed
			if(error){
				return false
			}
			else{
				// return true
				return data.isAdmin
			} 

		})
	}
	else{
		//If user is not an admin
		return false
	}
}

//Retrieve all courses 
module.exports.getAllCourses=()=>{
	return Course.find({}).then(result =>{
		return result
	})
}

//Retrieve all ACTIVE course
module.exports.getAllActive=()=>{
	return Course.find({isActive:true}).then(result=>{
		return result
	})
}

module.exports.getOneCourse=(reqBody)=>{
	return Course.findOne({name:reqBody.name}).then(result=>{
		return result
	})
}

//Update a course
// module.exports.updateCourse = (courseId, data) => {
// 	if(data.isAdmin){
// 		return Course.findById(courseId).then((result,err)=>{
// 			if(err){
// 				console.log(err)
// 				return false;
// 			}
// 			result.name = data.course.name
// 			result.description = data.course.description
// 			result.price = data.course.price
// 			result.isActive = data.course.isActive

// 			return result.save().then((updatedCourse,saveErr)=>{
// 				if(saveErr){
// 					console.log(saveErr)
// 					return false
// 				}
// 				else{
// 					return updatedCourse
// 				}
// 			})
// 		})
// 	}
// 	else{
// 		return false
// 	}
// }


//Update a Course v2.
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error)=>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}

module.exports.archiveCourse=(reqParams)=>{
	let updatedCourse = {
	isActive: false
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error)=>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}